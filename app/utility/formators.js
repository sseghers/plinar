export function convertNullData(value, toString = false) {
  if (value === '"null"') {
    return (toString)
      ? ""
      : undefined;
  }
  return value;
}
