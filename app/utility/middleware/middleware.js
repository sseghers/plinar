export default function middleware(middlewares = [], actionCreator, data) {
  if (middlewares.some(x => x)) {
    return middlewares.map((middlewareFunction) => {
      middlewareFunction.call(this);
      // Invoke and return the action when all middleware complete.
      if (middlewareFunction === middlewares.slice(-1)[0]) {
        return (data)
          ? actionCreator(data)
          : actionCreator();
      }
    })
    .pop(); // Return the action from map.
  }
  return actionCreator(data);
}
