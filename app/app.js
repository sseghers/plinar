import React from "react";
import { connect } from "react-redux";
import { render } from "react-dom";
import { Switch, Route } from "react-router-dom";
import { MuiThemeProvider, MuiTheme } from "material-ui/styles";

import PlateContainer from "./home/container/plateContainer";
import CreateItemContainer from "./shared/create-item/container/createItemContainer";

export default class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <Switch>
          <Route exact path="/" render={({ history }) => <PlateContainer history={history} /> } />
          <Route exact path="/:uid" render={({ history, match }) => <CreateItemContainer history={history} match={match} />} />
        </Switch>
      </MuiThemeProvider>
    );
  }
}
