import shortid from "shortid";

export const CREATE_TOPIC = "plinar/topic/CREATE_TOPIC";
export const GET_ALL = "plinar/topic/GET_ALL";
export const DELETE = "plinar/topic/DELETE";
export const UPDATE = "plinar/topic/UPDATE";

let initialState;
(function loadInitialState() {
  initialState = {
    topicsInitialized: false,
    byUid: {},
    allUids: [],
  };
})();

export function selectTopics(state) {
  return state.topics;
}

export function selectByUid(state) {
  return state.topics.byUid;
}

export function selectFirstByUId(state) {
  return state.topics.byUid[Object.keys(state.topics.byUid)[0]];
}

export function selectAllUids(state) {
  return state.topics.allUids;
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL:
      return state;
    case UPDATE:
      return Object.assign({}, state, selectTopics(action.payload), {
        topicsInitialized: true
      });
    case DELETE:
      const uid = action.payload.uid;
      const { [uid]: idFromById, ...newByUidState } = selectByUid(state);
      return {
        topics: {
          byUid: newByUidState,
          allUids: selectAllUids(state).filter(x => x !== uid)
        }
      };
    default:
      return state;
  }
}

function createTopic(
  uid = shortid.generate(),
  title,
  content,
  subtitle = "",
  creator = "",
  image = "",
  timestamp = Date.now()) {
  return {
    type: CREATE_TOPIC,
    payload: {
      uid,
      title,
      content,
      subtitle,
      creator,
      image,
      timestamp
    },
  };
}

function getAllTopics(pageSize = 10) {
  return {
    type: GET_ALL,
    payload: {
      pageSize
    }
  };
}

function deleteTopic(uid) {
  return {
    type: DELETE,
    payload: {
      uid
    }
  };
}

function updateTopics(newTopicsState) {
  return {
    type: UPDATE,
    payload: {
      ...newTopicsState.data
    }
  };
}

export const topicActions = {
  createTopic,
  getAllTopics,
  deleteTopic,
  updateTopics
};
