import { connect } from "react-redux";

import Topic from "./../component/topic";
import reducer, { topicActions } from "./../ducks-topic";

export const TopicContainer = connect()(Topic);

export default TopicContainer;
