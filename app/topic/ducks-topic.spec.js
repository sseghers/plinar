import reducer, * as topic from "./ducks-topic";

const { createTopic, getTopicByUid, getAllTopics, deleteTopic, updateTopic } = topic.topicActions;

describe("Feature Topic", () => {
  let sampleTopics;
  let initialState;

  beforeEach(() => {
    sampleTopics = [{
      uid: "TopicId1",
      creator: "CreatorId1",
      color: "ColorValue1",
      title: "Title1",
      subtitle: "Subtitle1",
      content: "Content1",
      image: "ImageBlobl1",
      timestamp: 1234
    },
    {
      uid: "TopicId2",
      creator: "CreatorId2",
      color: "ColorValue2",
      title: "Title2",
      subtitle: "Subtitle2",
      content: "Content2",
      image: "ImageBlobl2",
      timestamp: 12345
    },
    {
      uid: "TopicId3",
      creator: "CreatorId3",
      color: "ColorValue3",
      title: "Title3",
      subtitle: "Subtitle3",
      content: "Content3",
      image: "ImageBlobl3",
      timestamp: 123456
    }];

    initialState = {
      topics: {
        byUid: { },
        allUids: []
      }
    };
  });

  it("should handle initial state", () => {
    expect(reducer(undefined, {}))
      .toEqual(initialState);
  });

  it("should create a topic given no state", () => {
    const expectedItem = sampleTopics[0];
    initialState.topics.byUid[expectedItem.uid] = expectedItem;
    initialState.topics.allUids.push(expectedItem.uid);
    const expectedState = initialState;
    const topicAction = createTopic(expectedItem.uid,
      expectedItem.creator,
      expectedItem.color,
      expectedItem.title,
      expectedItem.subtitle,
      expectedItem.content,
      expectedItem.image,
      expectedItem.timestamp);

    expect(reducer(undefined, topicAction))
      .toEqual(expectedState);
  });

  it("should create the specified topic given initial state", () => {
    const firstItem = sampleTopics[0];
    initialState.topics.byUid[firstItem.uid] = firstItem;
    initialState.topics.allUids.push(firstItem.uid);
    const itemToCreate = sampleTopics[1];
    const expectedState = {
      topics: {
        byUid: {
          [firstItem.uid]: {
            ...firstItem
          },
          [itemToCreate.uid]: {
            ...itemToCreate
          }
        },
        allUids: [firstItem.uid, itemToCreate.uid]
      }
    };

    const topicAction = createTopic(itemToCreate.uid,
      itemToCreate.creator,
      itemToCreate.color,
      itemToCreate.title,
      itemToCreate.subtitle,
      itemToCreate.content,
      itemToCreate.image,
      itemToCreate.timestamp);

    expect(reducer(initialState, topicAction))
      .toEqual(expectedState);
  });

  it("should return an empty object when attempting a get given no state", () => {
    const topicAction = getTopicByUid(1);
    expect(reducer(undefined, topicAction))
      .toEqual({});
  });

  it("should get the specified topic given initial state", () => {
    const firstItem = sampleTopics[0];
    initialState.topics.byUid[firstItem.uid] = firstItem;
    initialState.topics.allUids.push(firstItem.uid);
    const secondItem = sampleTopics[1];
    initialState.topics.byUid[secondItem.uid] = secondItem;
    initialState.topics.allUids.push(secondItem.uid);
    const expectedState = secondItem;

    const topicAction = getTopicByUid(secondItem.uid);

    expect(reducer(initialState, topicAction))
      .toEqual(expectedState);
  });

  it("should get all topics given an initial state", () => {
    const expectedState = {
      topics: {
        byUid: {
          [sampleTopics[0].uid]: {
            ...sampleTopics[0]
          },
          [sampleTopics[1].uid]: {
            ...sampleTopics[1]
          },
          [sampleTopics[2].uid]: {
            ...sampleTopics[2]
          }
        },
        allUids: [sampleTopics[0].uid, sampleTopics[1].uid, sampleTopics[2].uid]
      }
    };
    initialState = expectedState;
    const topicAction = getAllTopics();
    expect(reducer(initialState, topicAction))
      .toEqual(expectedState);
  });

  it("should return an empty object when attempting to delete given no state", () => {
    const topicAction = deleteTopic(1);
    expect(reducer(undefined, topicAction))
      .toEqual(initialState);
  });

  it("should return a new state without the specified item given initial state", () => {
    const firstItem = sampleTopics[0];
    const secondItem = sampleTopics[1];
    const expectedState = {
      topics: {
        byUid: {
          [firstItem.uid]: {
            ...firstItem
          }
        },
        allUids: [firstItem.uid]
      }
    };
    initialState.topics.byUid[firstItem.uid] = firstItem;
    initialState.topics.allUids.push(firstItem.uid);
    initialState.topics.byUid[secondItem.uid] = secondItem;
    initialState.topics.allUids.push(secondItem.uid);

    const topicAction = deleteTopic(secondItem.uid);
    expect(reducer(initialState, topicAction))
      .toEqual(expectedState);
  });
});
