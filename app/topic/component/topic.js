import React from "react";
import PropTypes from "prop-types";
import Card, { CardActions, CardHeader, CardContent } from "material-ui/Card";
import Grid from "material-ui/Grid";

import { convertNullData } from "./../../utility/formators";

const Topic = ({ uid, title, subtitle, content, image, timestamp, opacity }) => (
    <Grid container className="custom_item_grid"
      style={{ opacity: `${opacity}` }}
      key={uid}>
      <Grid item xs={1} />
      <Grid item xs={10}>
        <Card className="column is-10 is-offset-1" key={uid}>
          <CardHeader
            title={`${title}`}
            subheader={convertNullData(subtitle, true)}
            key={uid} />
          <CardContent>
            {content}
          </CardContent>
        </Card>
      </Grid>
  </Grid>
    );

export default Topic;
