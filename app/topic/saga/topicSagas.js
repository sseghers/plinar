import { take, put, call, fork, select } from "redux-saga/effects";
import { topicActions, selectTopics, selectAllUids, GET_ALL, CREATE_TOPIC } from "./../ducks-topic";
import { loadingActions, selectPendingRequest } from "./../../shared/loading/loading";
import { fetchGetAllTopics, fetchPutTopic } from "./../../api/api";

const { getAllTopics, updateTopics } = topicActions;
const { createRequestSuccess, createRequestFail } = loadingActions;

function* fetchTopics() {
  const { response } = yield call(fetchGetAllTopics);
  if (response) {
    yield put(updateTopics(response));
  } else {
    yield put(createRequestFail("TOPIC", "Get all topics failed."));
  }
}

function* putTopic() {
  const request = yield select(selectPendingRequest);
  const { response } = yield call(fetchPutTopic, request.info);
  if (response) {
    yield put(createRequestSuccess("TOPIC", "Put topic complete."));
  } else {
    yield put(createRequestFail("TOPIC", "Put topic failed."));
  }
}

function* watchGetAllTopics() {
  while (true) {
    yield take(GET_ALL);
    yield fork(fetchTopics);
  }
}

function* watchPutTopic() {
  while (true) {
    yield take(CREATE_TOPIC);
    yield fork(putTopic);
  }
}

export default function topicSagas() {
  return [
    fork(watchGetAllTopics),
    fork(watchPutTopic)
  ];
}
