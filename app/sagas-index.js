import { all } from "redux-saga/effects";
import topicSagas from "./topic/saga/topicSagas";

export default function* rootSaga() {
  yield all([...topicSagas()]);
}
