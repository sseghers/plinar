import { combineReducers } from "redux";
import topicReducer from "./topic/ducks-topic";
import syncReducer from "./shared/sync-animation/sync-animation";
import loadingReducer from "./shared/loading/loading";

const rootReducer = combineReducers({
  topics: topicReducer,
  syncAnimation: syncReducer,
  requestState: loadingReducer
});

export default rootReducer;
