/* eslint import/first: 0 */
import React from "react";
import { render } from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";

import App from "./app";
import configureStore from "./configureStore";
import rootSaga from "./sagas-index";
import Globals from "./globals.scss";

const store = configureStore();
store.runSaga(rootSaga);

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
