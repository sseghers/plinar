import { GET_ALL, UPDATE } from "./../../topic/ducks-topic";

export const SYNC_RESET = "plinar/sync-animation/SYNC_RESET";
export const SYNC_NEXT_STEP = "plinar/sync-animation/SYNC_NEXT_STEP";

let initialState;
(function loadInitialState() {
  initialState = {
    syncIsFirstCycle: false,
    syncIsSecondCycle: false,
    opacity: 1,
    progress: 0,
    finishSyncQueued: false,
    syncRunning: false
  };
})();

export function selectSyncAnimation(state) {
  return state.syncAnimation;
}

function resetSync() {
  return {
    type: SYNC_RESET,
    payload: {
      ...initialState
    }
  };
}

function setNextStep(opacity, progress) {
  const syncIsFirstCycle = (progress < 0.5);
  const syncIsSecondCycle = !syncIsFirstCycle;

  return {
    type: SYNC_NEXT_STEP,
    payload: {
      syncIsFirstCycle,
      syncIsSecondCycle,
      opacity,
      progress,
      syncRunning: !(progress === 0.5 || progress > 1.01)
    }
  };
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL:
      return Object.assign({}, state, {
        syncIsFirstCycle: true
      });
    case UPDATE:
      return Object.assign({}, state, {
        finishSyncQueued: true
      });
    case SYNC_NEXT_STEP:
      return Object.assign({}, state, {
        opacity: action.payload.opacity,
        progress: action.payload.progress,
        syncRunning: action.payload.syncRunning,
        syncIsFirstCycle: action.payload.syncIsFirstCycle,
        syncIsSecondCycle: action.payload.syncIsSecondCycle
      });
    case SYNC_RESET:
      const test = Object.assign({}, state, action.payload);
      return test;
    default:
      return state;
  }
}

export const syncActions = {
  setNextStep,
  resetSync
};
