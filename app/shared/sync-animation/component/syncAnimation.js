import React from "react";
import { render } from "react-dom";

export default class SyncAnimation extends React.Component {
  state = {
    duration: 1000,
    fps: 60,
    scrollStep: 1 / ((1500 / 1000) * 60),
    takeStep: 1 / ((1500 / 1000) * 60),
    counter: 1,
    opacity: 1
  };

  resetSyncState() {
    this.state.counter = 1;
    this.state.takeStep = 1 / ((1500 / 1000) * 60);
    this.state.opacity = 1;
  }

  syncFirstCycle(animationStep) {
    if (this.state.takeStep < 0.5) {
      console.log(`Take step: ${this.state.takeStep} counter: ${this.state.counter} opacity: ${this.state.opacity}`);
      this.props.setNextStep(this.state.opacity, this.state.takeStep);
      this.state.counter += 2;
      this.state.takeStep = this.state.scrollStep * this.state.counter;
      this.state.opacity = 1 - (this.state.takeStep + 0.1);
      window.requestAnimationFrame(animationStep.bind(this));
    } else {
      this.props.setNextStep(this.state.opacity, this.state.takeStep);
    }
  }

  syncSecondCycle(animationStep) {
    if (this.state.takeStep >= 0.5
      && this.state.takeStep <= 1.0) {
      console.log(`Take step: ${this.state.takeStep} counter: ${this.state.counter} opacity: ${this.state.opacity}`);
      this.props.setNextStep(this.state.opacity, this.state.takeStep);
      this.state.counter += 2;
      this.state.takeStep = this.state.scrollStep * this.state.counter;
      this.state.opacity = (this.state.takeStep - this.state.scrollStep - 0.005);
      window.requestAnimationFrame(animationStep.bind(this));
    } else {
      this.props.resetSync();
      this.resetSyncState.call(this);
    }
  }

  render() {
    const { syncIsFirstCycle, syncIsSecondCycle,
      opacity, children, progress,
      finishSyncQueued, syncRunning } = this.props;
    if (syncIsFirstCycle && !syncRunning) {
      const animationStep = function() {
        setTimeout(() => {
          this.syncFirstCycle(animationStep);
        }, 1000 / this.state.fps);
      };
      window.requestAnimationFrame(animationStep.bind(this));
    }

    if (syncIsSecondCycle
        && !syncRunning
        && finishSyncQueued) {
      const animationStep = function() {
        setTimeout(() => {
          this.syncSecondCycle(animationStep);
        }, 1000 / this.state.fps);
      };
      window.requestAnimationFrame(animationStep.bind(this));
    }

    return (
      <div>
        {
          this.props.children
        }
      </div>
    );
  }
}
