import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import syncAnimation from "./../component/syncAnimation";
import syncReducer, { syncActions, selectSyncAnimation } from "./../sync-animation";

const { setNextStep, resetSync } = syncActions;

const mapStateToProps = (state) => {
  const { syncIsFirstCycle, syncIsSecondCycle,
      opacity, progress,
      finishSyncQueued, syncRunning } = selectSyncAnimation(syncReducer(state, {}));
  return {
    syncIsFirstCycle,
    syncIsSecondCycle,
    opacity,
    progress,
    finishSyncQueued,
    syncRunning
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    setNextStep,
    resetSync
  }, dispatch);
};

const SyncAnimationContainer = connect(
  mapStateToProps,
  mapDispatchToProps)(syncAnimation);

export default SyncAnimationContainer;
