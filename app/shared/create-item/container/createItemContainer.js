import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import CreateItem from "./../component/createItem";
import { topicActions } from "./../../../topic/ducks-topic";
import { requestIsSuccess } from "./../../loading/loading";
import { navigationActions } from "./../../navigation/navigation";

const { createTopic } = topicActions;
const { createRedirect } = navigationActions;

const mapStateToProps = (state) => {
  return {
    shouldRedirect: requestIsSuccess(state)
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    createTopic,
    createRedirect
  }, dispatch);
};

const CreateItemContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateItem);

export default CreateItemContainer;
