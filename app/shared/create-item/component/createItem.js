/* eslint no-param-reassign: 0 */

import React from "react";
import Paper from "material-ui/Paper";
import TextField from "material-ui/TextField";
import Button from "material-ui/Button";
import Grid from "material-ui/Grid";

import LoadingContainer from "./../../loading/container/loadingContainer";

export default class CreateItem extends React.Component {
  constructor(props) {
    super(props);
    this.submitItem = this.submitItem.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      title: {
        value: "",
        error: false
      },
      subtitle: {
        value: ""
      },
      content: {
        value: "",
        error: false,
      },
      image: {
        value: "",
      }
    };
  }

  submitItem(event) {
    if (this.formHasErrors()) {
      event.preventDefault();
      return;
    }
    this.props.createTopic(this.props.match.params.uid,
      this.state.title.value,
      this.state.content.value,
      this.state.subtitle.value);
  }

  formHasErrors() {
    let hasErrors = false;
    Object.keys(this.state).map((key) => {
      const item = Object.assign({}, this.state[key]);
      this.validateRequiredField(item.value, item);
      if (item.error) {
        hasErrors = true;
        this.updateState(key, item);
      }
    });
    return hasErrors;
  }

  updateState(id, state) {
    this.setState({
      [id]: state
    });
  }

  validateRequiredField(value, state) {
    if (state.error !== undefined && value === "") {
      state.error = true;
    } else {
      state.error = false;
    }
  }

  handleChange(event) {
    const updatedStateSlice = Object.assign({}, this.state[event.target.id]);
    this.validateRequiredField(event.target.value, updatedStateSlice);
    updatedStateSlice.value = event.target.value;
    this.updateState(event.target.id, updatedStateSlice);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.shouldRedirect) {
      this.props.createRedirect();
      this.props.history.push("/");
    }
  }

  render() {
    return (
    <div>
      <LoadingContainer />
      <Grid container className="custom_item_grid grid_create_item">
        <Grid item xs={1} />
          <Grid item xs={10}>
            <Paper>
              <Grid container>
                <Grid item xs={12}>
                  <form onSubmit={this.submitItem}>
                    <Grid container>
                      <Grid item xs={1} />
                      <Grid item xs={5}>
                        <TextField
                          id="title"
                          label="Title"
                          margin="normal"
                          onChange={this.handleChange}
                          error={this.state.title.error}
                          className="item_grid_text_field"/>
                      </Grid>
                      <Grid item xs={5}>
                        <TextField
                          id="subtitle"
                          label="Subtitle"
                          margin="normal"
                          onChange={this.handleChange}
                          className="item_grid_text_field"/>
                      </Grid>
                      <Grid item xs={1} />

                      <Grid item xs={12} />
                      <Grid item xs={12} />

                      <Grid item xs={1} />
                      <Grid item xs={10}>
                        <TextField multiline
                          id="content"
                          rowsMax="50"
                          onChange={this.handleChange}
                          error={this.state.content.error}
                          className=""
                          margin="normal"
                          className="item_grid_text_field"/>
                      </Grid>
                      <Grid item xs={1} />

                      <Grid item xs={11} />
                      <Grid item xs={1}>
                        <Button className="" onClick={this.submitItem}>
                          <i className="material-icons">send</i>
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
      </Grid>
    </div>
    );
  }
}
