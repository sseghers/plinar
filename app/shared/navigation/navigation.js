export const PAGE_REDIRECT = "plinar/navigation/PAGE_REDIRECT";

function createRedirect() {
  return {
    type: PAGE_REDIRECT,
    payload: {}
  };
}

export const navigationActions = {
  createRedirect
};
