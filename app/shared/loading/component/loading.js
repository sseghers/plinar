import React from "react";
import { CircularProgress } from "material-ui/Progress";

const Loading = ({ showLoader }) => (
  <div>
    { showLoader === true &&
    <CircularProgress size={100} className="create_item_loading"/>
    }
  </div>
);

export default Loading;
