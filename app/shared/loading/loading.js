import { CREATE_TOPIC } from "./../../topic/ducks-topic";
import { PAGE_REDIRECT } from "./../navigation/navigation";

export const REQUEST_SUCCESS = "plinar/loading/REQUEST_SUCCESS";
export const REQUEST_FAIL = "plinar/loading/REQUEST_FAIL";

let initialState;
(function loadInitialState() {
  initialState = {
    isPending: {
      type: "",
      info: "",
    },
    isSuccess: {
      type: "",
      info: "",
    },
    isError: {
      type: "",
      info: "",
    }
  };
})();

export function selectRequestState(state) {
  return state.requestState;
}

export function selectPendingRequest(state) {
  return state.requestState.isPending;
}

export function requestIsRunning(state) {
  return state.requestState.isPending.type !== ""
    && state.requestState.isPending.info !== "";
}

export function requestIsSuccess(state) {
  return state.requestState.isSuccess.type !== ""
    && state.requestState.isSuccess.info !== "";
}

export function requestIsError(state) {
  return state.requestState.isError.type !== ""
    && state.requestState.isError.info !== "";
}

export function requestIsComplete(state) {
  return requestIsSuccess(state) || requestIsError(state);
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_TOPIC:
      return Object.assign({}, state, {
        isPending: {
          type: "TOPIC",
          info: action.payload,
        },
        isSuccess: {
          type: "",
          info: "",
        },
        isError: {
          type: "",
          info: "",
        }
      });
    case REQUEST_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: {
          ...action.payload
        },
        isPending: {
          type: "",
          info: "",
        },
        isError: {
          type: "",
          info: "",
        }
      });
    case REQUEST_FAIL:
      return Object.assign({}, state, {
        isError: {
          ...action.payload
        },
        isSuccess: {
          type: "",
          info: "",
        },
        isPending: {
          type: "",
          info: "",
        }
      });
    case PAGE_REDIRECT:
      return Object.assign({}, state, initialState);
    default:
      return state;
  }
}

function createRequestSuccess(type, info) {
  return {
    type: REQUEST_SUCCESS,
    payload: {
      type,
      info
    }
  };
}

function createRequestFail(type, info) {
  return {
    type: REQUEST_FAIL,
    payload: {
      type,
      info
    }
  };
}

export const loadingActions = {
  createRequestSuccess,
  createRequestFail
};
