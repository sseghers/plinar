import React from "react";
import Button from "material-ui/Button";
import List, { ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";

const MenuActions = ({ createTopic, syncTopics }) => (
  <div className="itemActionsGroup">
    <List className="">
      <ListItem button onClick={createTopic}>
        <i className="material-icons">add</i>
      </ListItem>
      <Divider light />
      <ListItem button onClick={syncTopics}>
        <i className="material-icons">autorenew</i>
      </ListItem>
    </List>
  </div>
);

export default MenuActions;
