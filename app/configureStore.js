/* eslint no-underscore-dangle: 0 */
import { applyMiddleware, createStore, compose } from "redux";
import logger from "redux-logger";
import createSagaMiddleware, { END } from "redux-saga";

import rootReducer from "./reducers-index";

export default function configureStore(initialState = {}) {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middleware = [sagaMiddleware, logger]; // Logger should be last in the chain.

  const store = createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(...middleware)));

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);

  return store;
}
