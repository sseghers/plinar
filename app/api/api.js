import "whatwg-fetch";

const API_METHODS = {
  GET_ALL: "GET",
  PUT: "PUT"
};

const API_BASE_URL = "http://localhost:3000/api";

const constructUrl = (method, entity, body) => {
  switch (method) {
    case API_METHODS.GET_ALL:
      return `${API_BASE_URL}/${entity}`;
    case API_METHODS.PUT:
      return `${API_BASE_URL}/${entity}/${body.uid}`;
    default:
      return API_BASE_URL;
  }
};

function createFetchRequest(method, entity, body = {}) {
  let parsedBody = body;
  if (body.constructor === Object && Object.keys(body).length !== 0) {
    // parsedBody = new FormData();
    // Object.keys(body).map((property) => {
    //   parsedBody.append(property, body[property]);
    // });
    parsedBody = JSON.stringify(body);
  }

  return () => fetch(constructUrl(method, entity, body), {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: parsedBody
  });
}

export function fetchGetAllTopics() {
  const request = createFetchRequest(API_METHODS.GET_ALL, "topics");
  return request()
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return {
        response: {
          data
        }
      };
    });
}

export function fetchPutTopic(body) {
  const request = createFetchRequest(API_METHODS.PUT, "topics", body);
  return request()
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return {
        response: {
          data
        }
      };
    })
    .catch((error) => {
      return {
        error: {
          error
        }
      };
    });
}
