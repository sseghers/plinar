import React from "react";
import { render } from "react-dom";
import shortid from "shortid";

import MenuActions from "./../../shared/menu-actions/component/menuActions";
import SyncAnimationContainer from "./../../shared/sync-animation/container/syncAnimationContainer";
import TopicContainer from "./../../topic/container/topicContainer";

import plateStyle from "./../scss/plate.scss";

export default class Plate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      useSyncEffect: false
    };
    this.syncTopics = this.syncTopics.bind(this);
    this.createTopic = this.createTopic.bind(this);
    this.props.updateTopics();
  }

  createTopic() {
    const newUid = shortid.generate();

    this.props.history.push(`/${newUid}`);
  }

  syncTopics() {
    this.state.useSyncEffect = true;
    this.props.updateTopics();
  }

  render() {
    const { topicsInitialized, sortedTopics, opacity } = this.props;
    return (
      <div>
        <MenuActions createTopic={this.createTopic} syncTopics={this.syncTopics}/>
        <div className={`${plateStyle.platePositionBase}`}>
          { topicsInitialized === true &&
          <div className={`${plateStyle.plateBaseStyle}`}>
            <SyncAnimationContainer>
            {
              sortedTopics.map((topic) => {
                const { uid: currentTopic } = topic;
                currentTopic.opacity = opacity;
                return <TopicContainer key={currentTopic.uid}
                  {...currentTopic} />;
              })
            }
            </SyncAnimationContainer>
            </div>
          }
        </div>
      </div>
    );
  }
}
