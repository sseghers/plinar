import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Plate from "./../component/plate";
import topicReducer, { topicActions, selectTopics } from "./../../topic/ducks-topic";
import { selectSyncAnimation } from "./../../shared/sync-animation/sync-animation";

const { getAllTopics } = topicActions;

export function sortTopics(byUid) {
  const wrappedToArray = [];
  Object.keys(byUid).map((topic) => {
    wrappedToArray.push({
      uid: byUid[topic]
    });
  });

  const sortByUid = (a, b) => {
    return b[Object.keys(b)[0]].timestamp - a[Object.keys(a)[0]].timestamp;
  };

  const sortedTopics = wrappedToArray.sort(sortByUid);
  return sortedTopics;
}

const mapStateToProps = (state) => {
  const { topicsInitialized, byUid, allUids } = selectTopics(state);
  const { opacity } = selectSyncAnimation(state);
  return {
    topicsInitialized,
    sortedTopics: (topicsInitialized === true)
      ? sortTopics(byUid)
      : {},
    allUids: (topicsInitialized === true)
      ? allUids
      : [],
    opacity
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    updateTopics: getAllTopics
  }, dispatch);
};

const PlateContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Plate);

export default PlateContainer;
