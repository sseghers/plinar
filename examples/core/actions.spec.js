import * as actions from "./actions";

describe("todo actions", () => {
  it("createTodo should create ADD_TODO action", () => {
    expect(actions.createTodo("Use Redux")).toEqual({
      type: "ADD_TODO",
      id: 0,
      text: "Use Redux"
    });
  });

  it("createVisibilityFilter should create SET_VISIBILITY_FILTER action", () => {
    expect(actions.createVisibilityFilter("active")).toEqual({
      type: "SET_VISIBILITY_FILTER",
      filter: "active"
    });
  });

  it("createToggleTodo should create TOGGLE_TODO action", () => {
    expect(actions.createToggleTodo(1)).toEqual({
      type: "TOGGLE_TODO",
      id: 1
    });
  });
});
