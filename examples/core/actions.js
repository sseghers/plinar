
// Specific action types
export const ADD_TODO = "ADD_TODO";
export const TOGGLE_TODO = "TOGGLE_TODO";
export const SET_VISIBILITY_FILTER = "SET_VISIBILITY_FILTER";

// Related action types
export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_ACTIVE: "SHOW_ACTIVE"
};

let todoIdIndex = 0;
// Action creators
export function createTodo(text) {
  return {
    type: ADD_TODO,
    id: todoIdIndex++,
    text };
}

export function createVisibilityFilter(filter) {
  return {
    type: SET_VISIBILITY_FILTER,
    filter };
}

export function createToggleTodo(id) {
  return {
    type: TOGGLE_TODO,
    id };
}
