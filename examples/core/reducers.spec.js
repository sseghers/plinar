import * as actions from "./actions";
import todoApp from "./reducers";

describe("todo reducers", () => {
  it("should handle initial state", () => {
    expect(
      todoApp(undefined, {})
    ).toEqual({
      todos: [],
      visibilityFilter: actions.VisibilityFilters.SHOW_ALL
    });
  });


  it("should handle ADD_TODO", () => {
    const todoToAdd = {
      id: 0,
      text: "Test text",
      type: actions.ADD_TODO,
    };
    const expectedTodo = {
      completed: false,
      id: 0,
      text: "Test text",
    };

    expect(todoApp(undefined, todoToAdd))
    .toEqual({
      todos: [expectedTodo],
      visibilityFilter: actions.VisibilityFilters.SHOW_ALL
    });
  });
});

