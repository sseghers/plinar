import React from "react";
import { Provider } from "react-redux";
import { mount } from "enzyme";
import configureStore from "redux-mock-store";
import Footer from "./Footer";


describe("Components", () => {
  // const props = {
  //   addTodo: jest.fn()
  // };
  // <Comonent {...props} />
  let mockStore;
  let store;
  let footerComponentWrapper;

  beforeEach(() => {
    mockStore = configureStore();
    store = mockStore([]);
    footerComponentWrapper = mount(<Provider store={store}><Footer /></Provider>); // eslint-disable-line no-undef
  });

  describe("Footer", () => {
    it("should render self and subcomponents", () => {
      expect(footerComponentWrapper.find("p").length).toBe(1);
    });
  });
});
