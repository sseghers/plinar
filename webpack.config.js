/* eslint quote-props: 0 */
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CircularDependencyPlugin = require("circular-dependency-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const pathHelper = require("./config/path-helper");
const config = require("./server.config");

module.exports = ({
  entry: {
    // "webpack-dev-server/client?http://localhost:8080",
    // "webpack/hot/only-dev-server", // don"t reload on errors
    // "webpack/hot/dev-server", reload on errors
    app: pathHelper.root("app/index.js"),
    vendor: [
      "babel-polyfill",
      "material-ui",
      "react",
      "react-router-dom",
      "react-redux",
      "react-router",
      "redux",
      "redux-saga",
      "reselect"
    ]
  },
  output: Object.assign({
    path: pathHelper.root("dist")
  },
    {
      filename: "[name].js",
      chunkFilename: "[name].chunk.js"
    }),
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["react"]
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          quiet: true // Silence warnings in both console and browser
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader?modules&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader"
        }),
      },
      {
        test: /\.s(c|a)ss$/,
        exclude: [
          /node_modules/,
          path.resolve(pathHelper.root("app/globals.scss"))
        ],
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader?modules&importLoader=2&sourceMap&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader!sass-loader"
        }),
      },
      {
        test: /\.s(c|a)ss$/,
        include: [
          /node_modules/,
          path.resolve(pathHelper.root("app/globals.scss"))
        ],
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader!sass-loader!bulma-loader"
        }),
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|ico)$/,
        loader: "file-loader?name=[name].[ext]",
      },
      {
        test: /\.(jpg|png|gif)$/,
        loaders: [
          "file-loader",
          {
            loader: "image-webpack-loader",
            query: {
              progressive: true,
              optimizationLevel: 7,
              interlaced: false,
              pngquant: {
                quality: "65-90",
                speed: 4,
              },
            },
          },
        ],
      },
      {
        test: /\.html$/,
        loader: "html-loader",
      },
      {
        test: /\.json$/,
        loader: "json-loader",
      },
      {
        test: /\.(mp4|webm)$/,
        loader: "url-loader",
        query: {
          limit: 10000,
        },
      }],
  },
  plugins: [
    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    // new webpack.NamedModulesPlugin(),
    new CopyWebpackPlugin([
      {
        from: "favicon.ico",
        to: "favicon.ico"
      }
    ]),
    new ExtractTextPlugin({
      filename: "[name].css",
      allChunks: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      filename: "vendor.js"
    }),
    new HtmlWebpackPlugin({
      template: "index.html"
    }),
    // new CircularDependencyPlugin({
    //   exclude: /a\.js|node_modules/,
    //   failOnError: false
    // }),
    // new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin()
  ],
  resolve: {
    modules: ["app", "node_modules"],
    extensions: [
      ".js",
      ".jsx",
      ".react.js",
    ],
    mainFields: [
      "browser",
      "jsnext:main",
      "main",
    ],
  },
  devtool: "cheap-module-source-map", // '#inline-source-map'
  devServer: {
    historyApiFallback: true
  },
  target: "web",
  // performance: options.performance || {},
  externals: {
    "cheerio": "window",
    "react/addons": "react",
    "react/lib/ExecutionEnvironment": "react",
    "react/lib/ReactContext": "react",
    "config": JSON.stringify(config) // JSON.stringify(production ? require("./config.prod.json") : require("./config.dev.json"))
  }
});
